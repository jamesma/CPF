﻿using CPF.Drawing;
using System;
using System.Collections.Generic;
using System.Text;

namespace CPF.Drawing
{
    public class StringFormat
    {
        public TextAlignment Alignment { get; internal set; }
    }
}
